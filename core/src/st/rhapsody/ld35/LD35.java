package st.rhapsody.ld35;

import com.badlogic.gdx.*;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import st.rhapsody.ld35.audio.Audio;
import st.rhapsody.ld35.screen.GameScreen;
import st.rhapsody.ld35.screen.InstructionScreen;

public class LD35 extends Game {


	private static boolean startGame;
	private static boolean showInstructions;
	private Screen currentScreen;
	private AssetManager assetManager;

	public static int WIDTH = 480*2;
	public static int HEIGHT = 320*2;


	@Override
	public void create () {

		assetManager = new AssetManager();

		assetManager.load("wrong.ogg", Sound.class);
		assetManager.load("passed.ogg", Sound.class);
		assetManager.load("music.ogg", Sound.class);

		assetManager.finishLoading();



		Audio.init(assetManager.get("wrong.ogg", Sound.class), assetManager.get("passed.ogg", Sound.class), assetManager.get("music.ogg", Sound.class));

		currentScreen = new InstructionScreen();
		setScreen(currentScreen);

		Audio.music.loop();

	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void render () {



		currentScreen.render(Gdx.graphics.getDeltaTime());

		if (startGame){
			currentScreen = new GameScreen();
			setScreen(currentScreen);
			startGame = false;
		}

		if (showInstructions){
			currentScreen = new InstructionScreen();
			setScreen(currentScreen);
			showInstructions = false;
		}
	}

	public static void startGame() {
		startGame = true;
	}

	public static void showInstructions(){
		showInstructions = true;
	}
}
