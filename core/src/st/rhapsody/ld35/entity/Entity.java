package st.rhapsody.ld35.entity;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by nicklas on 16/04/16.
 */
public class Entity {

    protected ModelInstance modelInstance;
    protected Vector3 tmp = new Vector3();
    private Vector3 pos = new Vector3();
    protected boolean disposable = false;


    public Entity() {

    }


    public void tick(float delta){
        if (disposable){
            return;
        }
    }

    public void render(ModelBatch modelBatch, Environment environment){
        modelBatch.render(modelInstance, environment);
    }

    public void dispose() {
    }

    public boolean isDisposable() {
        return disposable;
    }

    public Vector3 getPosition() {
        modelInstance.transform.getTranslation(pos);
        return pos;
    }
}
