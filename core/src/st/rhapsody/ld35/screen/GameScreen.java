package st.rhapsody.ld35.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import st.rhapsody.ld35.LD35;
import st.rhapsody.ld35.level.Level;
import st.rhapsody.ld35.level.Starfield;
import static st.rhapsody.ld35.LD35.WIDTH;
import static st.rhapsody.ld35.LD35.HEIGHT;

/**
 * Created by nicklas on 16/04/16.
 */
public class GameScreen implements Screen {
    private ModelBatch modelBatch;
    private PerspectiveCamera camera;
    private Level level;
    private PerspectiveCamera playerCamera;
    private ModelBatch playerBatch;
    private BitmapFont bitmapFont;
    private SpriteBatch uiBatch;
    private GlyphLayout layout;
    private TextureAtlas hearts;
    private Sprite heartSprite;
    private Sprite halfHeartSprite;
    private SpriteBatch starBatch;
    private Sprite starSprite;
    private BitmapFont defaultFont;


    private BitmapFont defaultFontSmaller;
    private Starfield starfield;
    private Environment environment;


    @Override
    public void show() {
        starBatch = new SpriteBatch();
        modelBatch = new ModelBatch();
        playerBatch = new ModelBatch();
        uiBatch = new SpriteBatch();
        camera = createCamera();
        playerCamera = createCamera();

        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));


        starBatch = new SpriteBatch();

        starfield = new Starfield(new Texture(Gdx.files.internal("star.png")));


        restart();

        bitmapFont = new BitmapFont(Gdx.files.internal("numbers.fnt"), Gdx.files.internal("numbers.png"), false);
        layout = new GlyphLayout();

        defaultFont = new BitmapFont();
        defaultFontSmaller = new BitmapFont();

        Texture heartTexture = new Texture(Gdx.files.internal("heart.png"));
        Texture halfHeart = new Texture(Gdx.files.internal("half_heart.png"));
        Texture star = new Texture(Gdx.files.internal("star.png"));

        heartSprite = new Sprite(heartTexture);
        heartSprite.setScale(2.0f);
        halfHeartSprite = new Sprite(halfHeart);
        halfHeartSprite.setScale(2.0f);

        starSprite = new Sprite(star);

    }

    private PerspectiveCamera createCamera() {
        PerspectiveCamera camera = new PerspectiveCamera(67, WIDTH, HEIGHT);
        camera.position.set(0f, 0f, 0f);
        camera.lookAt(0, 0, 0);
        camera.near = 0.1f;
        camera.far = 4000;
        camera.update();

        return camera;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);


        if (!level.isPlayerDead()) {
            level.tick(delta);
            level.lookAtCurrentWall(camera, playerCamera, delta);
            camera.update();
        }else{
            starfield.shake(0,0);
            starfield.tilt(0,0);
        }


        starfield.tick(Gdx.graphics.getDeltaTime());

        starBatch.begin();
        starfield.render(starBatch);
        starBatch.end();



        modelBatch.begin(camera);
        level.render(modelBatch, environment);
        modelBatch.end();

        Gdx.gl.glClear(GL20.GL_DEPTH_BUFFER_BIT);

        playerCamera.update();
        playerBatch.begin(playerCamera);
        level.renderPlayer(playerBatch, environment);
        playerBatch.end();

        uiBatch.begin();
        drawWallsPassed();
        drawPlayerHealth();
        if (level.isPlayerDead()){
            drawGameOver();
            if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
                LD35.showInstructions();
            }
        }
        uiBatch.end();
    }

    private void restart() {
        level = new Level(camera, starfield);
    }

    private void drawGameOver() {
        layout.setText(defaultFont, "GAME OVER!");
        defaultFont.getData().setScale(5.0f);
        defaultFont.draw(uiBatch, layout, WIDTH/4, HEIGHT/2);

        layout.setText(defaultFontSmaller, "PRESS SPACE TO RESTART");

        defaultFontSmaller.draw(uiBatch, layout, WIDTH/2.5f, HEIGHT/4);
    }

    private void drawPlayerHealth() {

        if (level.isPlayerDead()){
            return;
        }

        int playerHealth = level.getPlayerHealth();
        float playerHalfHealth = level.getPlayerHalfHealth();

        int heartPosition = 0;
        for (int i = 0; i < playerHealth; i++) {
            heartPosition = WIDTH - (32 * (5 - i));
            heartSprite.setPosition(heartPosition, HEIGHT -32);
            heartSprite.draw(uiBatch);
        }

        if (playerHealth < 5 && playerHalfHealth == 0.5f){
            if (playerHealth == 0){
                heartPosition = WIDTH - (32 * 6);
            }
            halfHeartSprite.setPosition(heartPosition + 32, HEIGHT -32);
            halfHeartSprite.draw(uiBatch);
        }
    }

    private void drawWallsPassed() {
        layout.setText(bitmapFont, ""+level.getWallsPassed()+"");
        bitmapFont.getData().setScale(5.0f);
        bitmapFont.draw(uiBatch, layout, 8, HEIGHT-16);
    }

    @Override
    public void resize(int width, int height) {

    }


    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        level.dispose();
    }
}
