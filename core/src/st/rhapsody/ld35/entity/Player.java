package st.rhapsody.ld35.entity;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.math.Matrix4;
import st.rhapsody.ld35.graphic.shape.Shape;
import st.rhapsody.ld35.graphic.shape.Shapes;

/**
 * Created by nicklas on 16/04/16.
 */
public class Player extends Entity{
    private Shape currentShape;
    private int wallsPassed = 0;
    private int health = 3;
    private float halfHealth = 0.0f;
    private boolean dead;
    private int currentPlayerShapeForm;
    private Color currentColor = Color.CYAN;


    public void changeShape(int playerShapeForm) {
        currentShape = Shapes.getInstance().getShape(playerShapeForm, currentColor);
        currentPlayerShapeForm = playerShapeForm;
        modelInstance = new ModelInstance(currentShape.getModel());
        modelInstance.transform.setToTranslation(0,-2.0f,-10);
    }

    public Shape getShape() {
        return currentShape;
    }

    public void increaseWallPassed(){
        wallsPassed++;
    }

    public int getWallsPassed() {
        return wallsPassed;
    }

    public void increaseHealth(){
        halfHealth += 0.5f;
        if (halfHealth >= 1.0f && health < 5){
            health++;
            halfHealth = 0;
        }
    }

    public int getHealth() {
        return health;
    }

    public float getHalfHealth() {
        return halfHealth;
    }

    public void hurt(){


        if (health <= 0 && halfHealth == 0.5f){
            halfHealth = 0.0f;
        }

        health--;
        if (health <= 0){
            health = 0;
        }



        if (health == 0 && halfHealth != 0.5f){
            dead = true;
        }

    }

    public boolean isDead() {
        return dead;
    }

    public void render(ModelBatch modelBatch, Environment environment) {
        modelBatch.render(modelInstance, environment);
    }

    public void changeColor(Color shapeColor) {
        currentShape.getModel().dispose();
        currentShape = Shapes.getInstance().getShape(currentPlayerShapeForm, shapeColor);
        currentColor = shapeColor;
        Matrix4 transform = modelInstance.transform;
        modelInstance = new ModelInstance(currentShape.getModel());
        modelInstance.transform.set(transform);
    }
}
