package st.rhapsody.ld35.audio;

import com.badlogic.gdx.audio.Sound;

/**
 * Created by nicklas on 16/04/16.
 */
public class Audio {

    public static Sound wrong;
    public static Sound passed;
    public static Sound music;

    public static void init(Sound wrongSound, Sound passedSound, Sound musicSound){

        wrong = wrongSound;
        passed = passedSound;
        music = musicSound;

    }
}
