package st.rhapsody.ld35.graphic.shape;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.FloatAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.MathUtils;

/**
 * Created by nicklas on 16/04/16.
 */
public class Shapes {
    private ModelBuilder modelBuilder;
    private static Shapes instance = new Shapes().init();


    public Shapes init(){

        modelBuilder = new ModelBuilder();

        return this;
    }

    public static Shapes getInstance() {
        return instance;
    }

    public Shape getRandomShape(Color color){
        int length = Shape.Type.values().length;
        int random = MathUtils.random(0, length-1);
        Shape.Type type = Shape.Type.values()[random];
        return new Shape(type, color);
    }

    public Shape getShape(Shape.Type type, Color color){
        return new Shape(type,color);
    }

    public Shape getShape(int id, Color color){
        return new Shape(Shape.Type.values()[id],color);
    }

    protected Model createSphereModel(Color color) {
        color.a = 0.9f;
        return modelBuilder.createSphere(2,2,1,4,4,new Material(ColorAttribute.createDiffuse(color),new BlendingAttribute(1.0f),new FloatAttribute(FloatAttribute.AlphaTest, 0.0f)), VertexAttributes.Usage.ColorUnpacked | VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);

    }

    protected Model createBoxModel(Color color) {
        color.a = 0.9f;
        return modelBuilder.createBox(1, 1, 1, new Material(ColorAttribute.createDiffuse(color),new BlendingAttribute(1.0f),new FloatAttribute(FloatAttribute.AlphaTest, 0.0f)), VertexAttributes.Usage.ColorUnpacked | VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);
    }

    public Model createWall(Color color) {
        color.a = 0.9f;
        return modelBuilder.createBox(16, 8, 0.5f, new Material(ColorAttribute.createDiffuse(color),new BlendingAttribute(1.0f),new FloatAttribute(FloatAttribute.AlphaTest, 0.0f)), VertexAttributes.Usage.ColorUnpacked | VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);
    }

    protected Model createConeModel(Color color){
        color.a = 0.9f;
        return modelBuilder.createCone(2,2,2,16,new Material(ColorAttribute.createDiffuse(color),new BlendingAttribute(1.0f),new FloatAttribute(FloatAttribute.AlphaTest, 0.0f)),VertexAttributes.Usage.ColorUnpacked | VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);
    }

    protected Model createCapsuleModel(Color color){
        color.a = 0.9f;
        return modelBuilder.createCapsule(1,4,16, new Material(ColorAttribute.createDiffuse(color),new BlendingAttribute(1.0f),new FloatAttribute(FloatAttribute.AlphaTest, 0.0f)),VertexAttributes.Usage.ColorUnpacked | VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);
    }

    protected Model createCylinderModel(Color color){
        color.a = 0.9f;
        return modelBuilder.createCylinder(1,2,2,4,new Material(ColorAttribute.createDiffuse(color),new BlendingAttribute(1.0f),new FloatAttribute(FloatAttribute.AlphaTest, 0.0f)),VertexAttributes.Usage.ColorUnpacked | VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);
    }

}
