package st.rhapsody.ld35.graphic.shape;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;

/**
 * Created by nicklas on 16/04/16.
 */
public class Shape {

    private Type type;
    private Model model;

    public enum Type{
        SPHERE,
        BOX,
        CONE,
        CYLINDER
    }

    public Model getModel() {
        return model;
    }

    public Type getType() {
        return type;
    }

    public Shape(Type type, Color color) {
        this.type = type;
        switch (type){
            case BOX:
                model = Shapes.getInstance().createBoxModel(color);
                break;
            case CONE:
                model = Shapes.getInstance().createConeModel(color);
                break;
            case CYLINDER:
                model = Shapes.getInstance().createCylinderModel(color);
                break;
            case SPHERE:
                model = Shapes.getInstance().createSphereModel(color);
                break;
        }
    }
}
