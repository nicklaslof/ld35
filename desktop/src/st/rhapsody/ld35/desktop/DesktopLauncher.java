package st.rhapsody.ld35.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import st.rhapsody.ld35.LD35;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 480*2;
		config.height = 320*2;
		config.fullscreen = false;
		new LwjglApplication(new LD35(), config);
	}
}
