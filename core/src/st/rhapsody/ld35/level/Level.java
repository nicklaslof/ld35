package st.rhapsody.ld35.level;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import st.rhapsody.ld35.audio.Audio;
import st.rhapsody.ld35.entity.Entity;
import st.rhapsody.ld35.entity.Player;
import st.rhapsody.ld35.entity.Wall;
import st.rhapsody.ld35.graphic.shape.Shape;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by nicklas on 16/04/16.
 */
public class Level {


    private static ArrayList<Entity> entities;
    private static boolean changePlayerShape;
    private static boolean addNewAllAtEnd;
    private static Player player;
    private static ArrayList<Wall> walls;
    private static float cameraShake;
    private int playerShapeForm;
    private static Wall currentWall;

    private static final int NUMBER_OF_WALLS = 16;

    private static int wallsCreated = 0;


    private static int wallDistance = -30;
    public static float speed = 5;
    private Wall lookAtWall;
    private Vector3 lookPosition;
    private Vector3 lerpLookPosition = new Vector3();
    private static PerspectiveCamera camera;
    private Starfield starfield;

    public Level(PerspectiveCamera camera, Starfield starfield) {
        this.camera = camera;
        this.starfield = starfield;
        entities = new ArrayList<Entity>();
        walls = new ArrayList<Wall>(NUMBER_OF_WALLS);

        entities.clear();

        player = new Player();
        cameraShake = 0.0f;
        wallDistance = -30;
        speed = 5;

        addWalls();
        changePlayerShape(MathUtils.random(0, Shape.Type.values().length-1));

    }

    private static void addWalls() {
        while (walls.size() != NUMBER_OF_WALLS){
            int number = walls.size();
            addWall(wallDistance*number);

        }
    }

    private void changePlayerShape(int i) {
        changePlayerShape = true;
        playerShapeForm = i;
    }

    public void tick(float delta) {

        Iterator<Entity> iterator = entities.iterator();


        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_1)){
            changePlayerShape(0);
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_2)){
            changePlayerShape(1);
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_3)){
            changePlayerShape(2);
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_4)){
            changePlayerShape(3);
        }


        while (iterator.hasNext()){
            Entity entity = iterator.next();
            entity.tick(delta);
            if (entity.isDisposable()){
                entity.dispose();
                iterator.remove();
                if (entity instanceof Wall){
                    walls.remove(0);
                    ArrayList<Wall> newWalls = new ArrayList<Wall>(NUMBER_OF_WALLS);
                    for (Wall wall : walls) {
                        newWalls.add(wall);
                    }
                    walls = newWalls;
                }
            }
        }

        if (changePlayerShape){
            player.changeShape(playerShapeForm);
            changePlayerShape = false;
        }

        if (addNewAllAtEnd){
            addWall(wallDistance * NUMBER_OF_WALLS);
            addNewAllAtEnd = false;
        }

        currentWall = walls.get(0);
    }

    private static void addWall(int z) {
        wallsCreated++;

        float sin = MathUtils.sin(wallsCreated+MathUtils.random(-2,2))*20;
        float cos = MathUtils.cos(wallsCreated+MathUtils.random(-2,2))*20;

        int x = (int) sin;
        int y = (int) cos;

        Wall wall = new Wall(x, y, z);
        walls.add(wall);

        entities.add(wall);
    }

    public void render(ModelBatch modelBatch, Environment environment) {

        for (Entity entity : entities) {
            entity.render(modelBatch, environment);
        }
    }

    public void dispose() {
        for (Entity entity : entities) {
            entity.dispose();
        }
    }

    public static void addNewWallAtEnd() {
        addNewAllAtEnd = true;
    }

    public void lookAtCurrentWall(PerspectiveCamera camera, PerspectiveCamera playerCamera, float deltaTime) {



        float lerp = Math.min(5.0f ,1.2f*(speed/5));
        //System.out.println(lerp);
        Vector3 position = camera.position;
        float x = (currentWall.getShapePosition().x - position.x) * lerp * deltaTime;
        float y = ((currentWall.getShapePosition().y+0.5f) - position.y) * lerp * deltaTime;
        position.x += x;
        position.y += y;

        starfield.tilt(x*8,y*8);


        if (cameraShake > 0.0f){
            x = MathUtils.sin(cameraShake)*MathUtils.random(1,3);
            y = MathUtils.cos(cameraShake)*MathUtils.random(1,3)-1;
            camera.position.x += x;
            camera.position.y += y;
            starfield.shake(x*8,y*8);
            cameraShake /= 1.3f;
        }
            if (cameraShake < 0.01f){
                cameraShake = 0.0f;
            }

    }

    public void renderPlayer(ModelBatch playerBatch, Environment environment) {
        player.render(playerBatch, environment);
    }

    public static void passingTroughWall() {
        if (currentWall != null && player != null && player.getShape() != null)
        if (!currentWall.matches(player.getShape())){
            Audio.wrong.play();
            player.hurt();
            cameraShake = 2.0f;

        }else{
            Audio.passed.play();
            player.increaseHealth();
            player.changeColor(currentWall.getShapeColor());
        }

        speed += 1.2f;
        wallDistance -= 1;
        player.increaseWallPassed();

    }

    public int getWallsPassed() {
        return player.getWallsPassed();
    }

    public int getPlayerHealth() {
        return player.getHealth();
    }

    public float getPlayerHalfHealth() {
        return player.getHalfHealth();
    }

    public boolean isPlayerDead() {
        return player.isDead();
    }
}
