package st.rhapsody.ld35.entity;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import st.rhapsody.ld35.graphic.shape.Shape;
import st.rhapsody.ld35.graphic.shape.Shapes;
import st.rhapsody.ld35.level.Level;

/**
 * Created by nicklas on 16/04/16.
 */
public class Wall extends Entity{


    private final ModelInstance shapeInstance;
    private Color color;
    private boolean wallAdded = false;
    private Shape shape;
    private boolean lookedAt;
    private Vector3 shapePosition = new Vector3();


    public Wall(int x, int y, int z) {
        super();

        color = null;

        while (color == null){
            color = new Color(MathUtils.random(), MathUtils.random(), MathUtils.random(), 1);
            if (color.r + color.g + color.b < 0.5f){
                color = null;
            }
        }

        Model wall = Shapes.getInstance().createWall(color);
        shape = Shapes.getInstance().getRandomShape(Color.BLACK);

        modelInstance = new ModelInstance(wall);

        modelInstance.transform.setToTranslation(x,y,z);

        shapeInstance = new ModelInstance(shape.getModel());
        shapeInstance.transform.setToTranslation(x ,y, z+0.5f);
        //System.out.println("new wall at "+x+" "+y+" "+z);
    }

    @Override
    public void tick(float delta) {
        super.tick(delta);

        modelInstance.transform.translate(0,0,delta*Level.speed);
        shapeInstance.transform.translate(0,0,delta*Level.speed);

        modelInstance.transform.getTranslation(tmp);

        if (tmp.z > -1){
            disposable = true;
            Level.addNewWallAtEnd();
            Level.passingTroughWall();
        }
    }

    @Override
    public void render(ModelBatch modelBatch, Environment environment) {
        super.render(modelBatch, environment);
        modelBatch.render(shapeInstance);
    }


    public Vector3 getShapePosition() {
        shapeInstance.transform.getTranslation(shapePosition);
        return shapePosition;
    }

    public boolean matches(Shape inShape) {
        return inShape.getType().equals(shape.getType());
    }


    public Color getShapeColor() {
        return color;
    }
}
