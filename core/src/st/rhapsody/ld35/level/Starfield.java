package st.rhapsody.ld35.level;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import static st.rhapsody.ld35.LD35.WIDTH;
import static st.rhapsody.ld35.LD35.HEIGHT;

/**
 * Created by nicklas on 17/04/16.
 */
public class Starfield {

    public static int depth = 900;

    private Vector3[] starLocations = new Vector3[200];
    private Sprite[] sprites = new Sprite[200];
    private Texture star;
    private float tilyY;
    private float tiltX;
    private float shakeX;
    private float shakeY;

    public Starfield(Texture star) {

        this.star = star;

        for (int i = 0; i < starLocations.length; i++) {
            if (starLocations[i] == null){
                starLocations[i] = new Vector3();
                sprites[i] = new Sprite(star);
            }
            Vector3 starLocation = starLocations[i];

            starLocation.set(MathUtils.random(-WIDTH,WIDTH), MathUtils.random(-HEIGHT,HEIGHT), MathUtils.random(0, depth));
        }

    }

    public void tilt(float x, float y){
        this.tiltX = x;
        this.tilyY = y;
    }

    public void shake(float x, float y){
        this.shakeX = x;
        this.shakeY = y;
    }

    public void tick(float delta){
        for (int i = 0; i < starLocations.length; i++) {
            Vector3 starLocation = starLocations[i];
            starLocation.z = starLocation.z - (delta*100);
            starLocation.x = starLocation.x - tiltX;
            starLocation.y = starLocation.y - tilyY;
            if (starLocation.z < 0){
                starLocation.set(MathUtils.random(-WIDTH,WIDTH), MathUtils.random(-HEIGHT,HEIGHT), MathUtils.random(0, depth));
            }
        }
    }

    public void render(SpriteBatch batch){
        for (int i = 0; i < starLocations.length; i++) {
            Vector3 starLocation = starLocations[i];
            float zz = 64 / starLocation.z;
            float x = (starLocation.x * zz + WIDTH/2.0f);
            float y = (starLocation.y * zz + HEIGHT/2.0f);
            Sprite sprite = sprites[i];
            //System.out.println(starLocation.z +" "+zz);
            float color = Math.min(1.0f,zz*1.5f);
            //float color = (1.0f / starLocation.z)*30;
            sprite.setScale(Math.max(0.3f,zz)*3);
            sprite.setColor(color,color,color,1);
            sprite.setPosition(x+shakeX,y+shakeY);
//            batch.draw(star,x+shakeX,y+shakeY);
            sprite.draw(batch);
        }

    }
}
