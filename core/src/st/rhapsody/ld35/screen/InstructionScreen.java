package st.rhapsody.ld35.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.math.Vector3;
import st.rhapsody.ld35.LD35;
import st.rhapsody.ld35.graphic.shape.Shape;
import st.rhapsody.ld35.graphic.shape.Shapes;
import st.rhapsody.ld35.level.Starfield;

import java.util.ArrayList;

/**
 * Created by nicklas on 17/04/16.
 */
public class InstructionScreen implements Screen {

    private SpriteBatch uiBatch;
    private BitmapFont defaultFont;
    private GlyphLayout layout;

    public static int width = 480*2;
    public static int height = 320*2;
    private Sprite logoSprite;
    private ModelBatch modelBatch;
    private PerspectiveCamera camera;

    private SpriteBatch starBatch;
    private Starfield starfield;

    private ArrayList<ModelInstance> models = new ArrayList<ModelInstance>();
    private Environment environment;


    @Override
    public void show() {
        layout = new GlyphLayout();
        modelBatch = new ModelBatch();

        camera = createCamera();

        defaultFont = new BitmapFont();
        uiBatch = new SpriteBatch();

        starBatch = new SpriteBatch();

        starfield = new Starfield(new Texture(Gdx.files.internal("star.png")));

        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));


        Texture texture = new Texture(Gdx.files.internal("ugly_logo.png"));
        logoSprite = new Sprite(texture);

        Shape shape = Shapes.getInstance().getShape(Shape.Type.SPHERE, Color.RED);
        ModelInstance modelInstance = new ModelInstance(shape.getModel());
        modelInstance.transform.setToTranslation(-11,5,-20);
        models.add(modelInstance);

        shape = Shapes.getInstance().getShape(Shape.Type.BOX, Color.GREEN);
        modelInstance = new ModelInstance(shape.getModel());
        modelInstance.transform.setToTranslation(-8,5,-20);
        models.add(modelInstance);

        shape = Shapes.getInstance().getShape(Shape.Type.CYLINDER, Color.YELLOW);
        modelInstance = new ModelInstance(shape.getModel());
        modelInstance.transform.setToTranslation(-5,5,-20);
        models.add(modelInstance);

        shape = Shapes.getInstance().getShape(Shape.Type.CONE, Color.LIGHT_GRAY);
        modelInstance = new ModelInstance(shape.getModel());
        modelInstance.transform.setToTranslation(-2,5,-20);
        models.add(modelInstance);
    }

    private PerspectiveCamera createCamera() {
        PerspectiveCamera camera = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(0f, 0f, 0f);
        camera.lookAt(0, 0, 0);
        camera.near = 0.1f;
        camera.far = 4000;
        camera.update();

        return camera;
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);


        starfield.tick(Gdx.graphics.getDeltaTime());

        starBatch.begin();
        starfield.render(starBatch);
        starBatch.end();



        for (ModelInstance model : models) {
            model.transform.rotate(Vector3.X, delta*100);
            model.transform.rotate(Vector3.Y, delta*50);
            model.transform.rotate(Vector3.Z, delta*80);
        }

        modelBatch.begin(camera);
        modelBatch.render(models, environment);
        modelBatch.end();



        uiBatch.begin();


        logoSprite.setPosition(50,width/2);
        logoSprite.draw(uiBatch);


        layout.setText(defaultFont, "* Pass as many walls as possible with a matching shape.");
        defaultFont.getData().setScale(1.0f);
        defaultFont.draw(uiBatch, layout, width/6, height/2);

        layout.setText(defaultFont, "* Select shape with 1-4");
        defaultFont.getData().setScale(1.0f);
        defaultFont.draw(uiBatch, layout, width/6, (height/2)-32);

        layout.setText(defaultFont, "* Press space to start");
        defaultFont.getData().setScale(1.0f);
        defaultFont.draw(uiBatch, layout, width/6, (height/2)-64);

        layout.setText(defaultFont, "Created for Ludum Dare 35 jam by:");
        defaultFont.getData().setScale(1.0f);
        defaultFont.draw(uiBatch, layout, width/6, (height/2)-(96+64));

        layout.setText(defaultFont, "Mathias Lindfeldt : Ideas, Graphic and Music");
        defaultFont.getData().setScale(1.0f);
        defaultFont.draw(uiBatch, layout, width/6, (height/2)-(128+64));

        layout.setText(defaultFont, "Nicklas Löf: Ideas, Programming, Graphic and Sound");
        defaultFont.getData().setScale(1.0f);
        defaultFont.draw(uiBatch, layout, width/6, (height/2)-(160+64));


        uiBatch.end();

        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
            LD35.startGame();
        }

    }

    @Override
    public void resize(int width, int height) {



    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
